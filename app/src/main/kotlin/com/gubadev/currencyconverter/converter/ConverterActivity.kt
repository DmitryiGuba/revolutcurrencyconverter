package com.gubadev.currencyconverter.converter

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.gubadev.currencyconverter.R
import com.gubadev.currencyconverter.adapter.CurrencyAdapter
import com.gubadev.currencyconverter.ui.BaseActivity
import com.gubadev.currencyconverter.viewmodel.injectViewModel
import com.gubadev.extencions.darkMode
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_converter.*
import javax.inject.Inject

class ConverterActivity : BaseActivity() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    lateinit var currencyViewModel: ConverterViewModel

    private val manager = LinearLayoutManager(this)
    private val adapter: CurrencyAdapter by lazy { CurrencyAdapter(currencyViewModel, manager) }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        currencyViewModel = injectViewModel(factory)
        darkMode = currencyViewModel.isNightMode()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_converter)
        setSupportActionBar(toolbar.apply { title = getString(R.string.converter) })
        list.let {
            it.layoutManager = manager
            it.adapter = adapter
            it.itemAnimator?.changeDuration = 0
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item ?: return false
        return when (item.itemId) {
            R.id.night_action -> {
                currencyViewModel.nightModeChanged(!darkMode)
                toggle()
                true
            }
            else -> false
        }
    }

}
