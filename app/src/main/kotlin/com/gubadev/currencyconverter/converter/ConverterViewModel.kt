package com.gubadev.currencyconverter.converter

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gubadev.data.entities.currencyCode
import com.gubadev.domain.entities.Currency
import com.gubadev.domain.entities.CurrencyCodes
import com.gubadev.domain.usecases.CurrencyUpdateUseCase
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ConverterViewModel @Inject constructor(
    private val updateUseCase: CurrencyUpdateUseCase,
    private val preferences: SharedPreferences
) : ViewModel() {

    val currencyData = MutableLiveData<List<Currency>>()

    private val amountRelay = BehaviorSubject.createDefault(defaultAmount())
    private lateinit var networkData: Observable<List<Currency>>
    private var subscribtion: Disposable? = null

    init {
        fetchCurrencies(defaultCurrency(), null)
    }

    fun isNightMode() = preferences.getBoolean(NIGHT_MODE_KEY, false)

    fun baseCurrencyChanged(base: String, newAmount: Float) {
        preferences.edit().apply {
            putString(BASE_CURRENCY_KEY, base)
            putFloat(BASE_CURRENCY_AMOUNT_KEY, newAmount)
            apply()
        }
        fetchCurrencies(base, newAmount)
    }

    fun baseCurrencyAmountChanged(amount: Float) {
        preferences.edit().apply {
            putFloat(BASE_CURRENCY_AMOUNT_KEY, amount)
            apply()
        }
        amountRelay.onNext(amount)
    }

    fun restoreOrder(): MutableList<CurrencyCodes>? {
        val stringedOrder = preferences.getString(CURRENCY_ORDER_KEY, "")
        if (stringedOrder.isNullOrEmpty()) return null
        return stringedOrder.split('|')
            .asSequence()
            .filter { it.isNotBlank() }
            .map { it.currencyCode() }
            .toMutableList()
    }

    fun nightModeChanged(isEnabled: Boolean) {
        preferences.edit().apply {
            putBoolean(NIGHT_MODE_KEY, isEnabled)
            apply()
        }
    }

    fun orderChanged(order: List<CurrencyCodes>) {
        if (order.isEmpty()) return
        val orderString = StringBuilder().run {
            order.forEach { append(it.name).append('|') }
            toString()
        }

        preferences.edit().apply {
            putString(CURRENCY_ORDER_KEY, orderString)
            apply()
        }
    }

    private fun fetchCurrencies(base: String, amount: Float?) {
        subscribtion?.apply { if (!isDisposed) dispose() }

        amount?.let { amountRelay.onNext(it) }

        networkData = Observable.interval(1, TimeUnit.SECONDS)
            .flatMap { updateUseCase.update(base).onErrorComplete().toObservable() }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

        subscribtion = Observables.combineLatest(
            networkData,
            amountRelay
        ) { values, amountData ->
            values.forEach { it.amount = it.value * amountData }
            values
        }
            .subscribe(
                {currencyData.value = it},
                { Log.e("CHRES", "${it.message}")}
            )
    }

    private fun defaultCurrency() = preferences.getString(BASE_CURRENCY_KEY, BASE_CURRENCY)!!

    private fun defaultAmount() = preferences.getFloat(BASE_CURRENCY_AMOUNT_KEY, 1f)

    private companion object {
        const val BASE_CURRENCY_KEY = "BASE_CURRENCY_KEY"
        const val BASE_CURRENCY_AMOUNT_KEY = "BASE_CURRENCY_AMOUNT_KEY"
        const val CURRENCY_ORDER_KEY = "CURRENCY_ORDER_KEY"
        const val NIGHT_MODE_KEY = "NIGHT_MODE_KEY"
        const val BASE_CURRENCY = "EUR"
    }
}