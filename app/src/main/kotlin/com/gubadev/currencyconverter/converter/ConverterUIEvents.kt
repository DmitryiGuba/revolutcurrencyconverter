package com.gubadev.currencyconverter.converter

import com.gubadev.domain.entities.Currency

sealed class ConverterUIEvents {
    data class NightModeChanged(val isNight: Boolean) : ConverterUIEvents()
    data class SortDialogOpened(val isOpened: Boolean): ConverterUIEvents()
    data class SortOrderChanged(val type: SortOrder) : ConverterUIEvents()
    data class MainCurrencyChanged(val currency: Currency) : ConverterUIEvents()
    data class CurrencyValueChanged(val value: Float) : ConverterUIEvents()
    data class NetworkStateChanged(val isAvaliable: Boolean) : ConverterUIEvents()
}

enum class SortOrder {
    TITLE, ASCENDING, DESCENDING
}