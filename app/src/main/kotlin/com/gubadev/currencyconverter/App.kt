package com.gubadev.currencyconverter

import android.app.Activity
import android.app.Application
import com.gubadev.currencyconverter.di.AppModule
import com.gubadev.currencyconverter.di.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var injector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
            .application(this)
            .baseUrl(BASE_API_URL)
            .appModule(AppModule)
            .build()
            .inject(this)
    }

    override fun activityInjector() = injector

    companion object {
        const val BASE_API_URL = "https://revolut.duckdns.org/"
    }
}


