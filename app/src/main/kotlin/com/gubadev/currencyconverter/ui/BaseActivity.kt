package com.gubadev.currencyconverter.ui

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import com.gubadev.extencions.darkMode

@SuppressLint("Registered")
open class BaseActivity: AppCompatActivity()  {

    protected fun toggle(){
        darkMode = !darkMode
    }
}