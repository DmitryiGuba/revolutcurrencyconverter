package com.gubadev.currencyconverter.adapter

import android.content.Context
import android.inputmethodservice.InputMethodService
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gubadev.currencyconverter.R
import com.gubadev.currencyconverter.converter.ConverterViewModel
import com.gubadev.domain.entities.Currency
import com.gubadev.domain.entities.CurrencyCodes
import com.gubadev.extencions.color
import com.gubadev.extencions.inflate
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item.*
import java.text.DecimalFormat

class CurrencyAdapter(
    private val viewModel: ConverterViewModel,
    val manager: LinearLayoutManager
) : RecyclerView.Adapter<CurrencyAdapter.CurrencyItemHolder>() {
    var data = mutableMapOf<CurrencyCodes, Currency>()
    var orderedCodes = mutableListOf<CurrencyCodes>()
    var listener: TextWatcher? = null

    private val baseCurrencyRelay = PublishSubject.create<CurrencyCodes>()
    private var currensyRelaySubscription: Disposable? = null

    init {
        viewModel.currencyData.observeForever {
            if (data.isEmpty()) {
                data = it.associateBy({ currency -> currency.code }, { currency -> currency }).toMutableMap()
                val order = viewModel.restoreOrder()
                if (order != null && !order.isEmpty()) {
                    orderedCodes = order
                } else {
                    it.map { currency -> currency.code }.toCollection(orderedCodes)
                }
            } else {
                it.forEach { currency ->
                    data[currency.code] = currency
                    if (orderedCodes[0] != currency.base) {
                        notifyDataSetChanged()
                    } else {
                        if (currency.base != currency.code) {
                            notifyItemChanged(orderedCodes.indexOf(currency.code))
                        }
                    }
                }
            }
            baseCurrencyRelay.onNext(it[0].base)
        }

        currensyRelaySubscription = baseCurrencyRelay
            .distinctUntilChanged()
            .subscribe({
                notifyDataSetChanged()
            }, { Log.e("CHRES A", "${it.message}") })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CurrencyItemHolder(parent.context.inflate(R.layout.list_item, parent))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: CurrencyItemHolder, position: Int) = holder.bind(position)

    inner class CurrencyItemHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bind(position: Int) {
            data[orderedCodes[position]]?.let { currency ->
                currency.getContentByCode().let {
                    item_country_code.text = it.first
                    item_country_name.setText(it.second)
                    item_icon.setImageResource(it.third)
                }
                item_currency_amount.apply {
                    removeTextChangedListener(listener)
                    setText(DecimalFormat("#.##").format(currency.amount), TextView.BufferType.EDITABLE)
                    if (text.isNotBlank()) {
                        setSelection(text.length)
                    }
                    if (currency.base == currency.code) {
                        listener = object : TextWatcher {
                            override fun afterTextChanged(s: Editable?) {}

                            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                                if (validateAmountString(s.toString())) {
                                    setTextColor(context.color(R.color.colorText))
                                    if (s.isNullOrBlank() || s == "0") {
                                        hint = "0"
                                        currency.amount = 0f
                                    } else {
                                        currency.amount = s.toString().replace(',', '.').toFloat()
                                    }

                                    viewModel.baseCurrencyAmountChanged(currency.amount)
                                } else {
                                    setTextColor(context.color(R.color.colotTextInvalid))
                                }
                            }
                        }
                        requestFocus()
                        addTextChangedListener(listener)
                    }
                }
            }

            containerView.setOnClickListener {
                handleCurrencySelect()
            }
        }

        private fun validateAmountString(amountString: String): Boolean {
            return amountString.matches("^(\\d*[.,]?\\d*)$".toRegex())
        }

        private fun handleCurrencySelect() {
            val actual = orderedCodes[adapterPosition]
            data[actual]?.let {
                orderedCodes.removeAt(adapterPosition)
                orderedCodes.add(0, actual)
                viewModel.orderChanged(orderedCodes)
                viewModel.baseCurrencyChanged(it.code.name, it.amount)
                notifyItemMoved(adapterPosition, 0)
                manager.scrollToPositionWithOffset(0, 0)
            }
        }
    }

    companion object {
        const val CURRENCY_VALUE_TEMPLATE = "%.2f"
    }
}

private fun Currency.getContentByCode() = when (this.code) {
    CurrencyCodes.EUR -> Triple("EUR", R.string.EUR, R.drawable.vd_european_union)
    CurrencyCodes.AUD -> Triple("AUD", R.string.AUD, R.drawable.vd_australia)
    CurrencyCodes.BGN -> Triple("BGN", R.string.BGN, R.drawable.vd_bulgaria)
    CurrencyCodes.BRL -> Triple("BRL", R.string.BRL, R.drawable.vd_brazil)
    CurrencyCodes.CAD -> Triple("CAD", R.string.CAD, R.drawable.vd_canada)
    CurrencyCodes.CHF -> Triple("CHF", R.string.CHF, R.drawable.vd_switzerland)
    CurrencyCodes.CNY -> Triple("CNY", R.string.CNY, R.drawable.vd_china)
    CurrencyCodes.CZK -> Triple("CZK", R.string.CZK, R.drawable.vd_czech_republic)
    CurrencyCodes.DKK -> Triple("DKK", R.string.DKK, R.drawable.vd_denmark)
    CurrencyCodes.GBP -> Triple("GBP", R.string.GBP, R.drawable.vd_united_kingdom)
    CurrencyCodes.HKD -> Triple("HKD", R.string.HKD, R.drawable.vd_hong_kong)
    CurrencyCodes.HRK -> Triple("HRK", R.string.HRK, R.drawable.vd_croatia)
    CurrencyCodes.HUF -> Triple("HUF", R.string.HUF, R.drawable.vd_hungary)
    CurrencyCodes.IDR -> Triple("IDR", R.string.IDR, R.drawable.vd_indonesia)
    CurrencyCodes.ILS -> Triple("ILS", R.string.ILS, R.drawable.vd_israel)
    CurrencyCodes.INR -> Triple("INR", R.string.INR, R.drawable.vd_india)
    CurrencyCodes.ISK -> Triple("ISK", R.string.ISK, R.drawable.vd_iceland)
    CurrencyCodes.JPY -> Triple("JPY", R.string.JPY, R.drawable.vd_japan)
    CurrencyCodes.KRW -> Triple("KRW", R.string.KRW, R.drawable.vd_south_korea)
    CurrencyCodes.MXN -> Triple("MXN", R.string.MXN, R.drawable.vd_mexico)
    CurrencyCodes.MYR -> Triple("MYR", R.string.MYR, R.drawable.vd_malaysia)
    CurrencyCodes.NOK -> Triple("NOK", R.string.NOK, R.drawable.vd_norway)
    CurrencyCodes.NZD -> Triple("NZD", R.string.NZD, R.drawable.vd_new_zealand)
    CurrencyCodes.PHP -> Triple("PHP", R.string.PHP, R.drawable.vd_philippines)
    CurrencyCodes.PLN -> Triple("PLN", R.string.PLN, R.drawable.vd_poland)
    CurrencyCodes.RON -> Triple("RON", R.string.RON, R.drawable.vd_romania)
    CurrencyCodes.RUB -> Triple("RUB", R.string.RUB, R.drawable.vd_russia)
    CurrencyCodes.SEK -> Triple("SEK", R.string.SEK, R.drawable.vd_sweden)
    CurrencyCodes.SGD -> Triple("SGD", R.string.SGD, R.drawable.vd_singapore)
    CurrencyCodes.THB -> Triple("THB", R.string.THB, R.drawable.vd_thailand)
    CurrencyCodes.TRY -> Triple("TRY", R.string.TRY, R.drawable.vd_turkey)
    CurrencyCodes.USD -> Triple("USD", R.string.USD, R.drawable.vd_united_states)
    CurrencyCodes.ZAR -> Triple("ZAR", R.string.ZAR, R.drawable.vd_south_africa)
}