package com.gubadev.currencyconverter.di

import com.gubadev.currencyconverter.converter.ConverterActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface ConverterActivityComponent : AndroidInjector<ConverterActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<ConverterActivity>() {}
}