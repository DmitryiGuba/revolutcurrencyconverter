package com.gubadev.currencyconverter.di

import android.app.Activity
import com.gubadev.currencyconverter.converter.ConverterActivity
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class ActivityModule {

    @Binds
    @IntoMap
    @ActivityKey(ConverterActivity::class)
    abstract fun bindConverterActivity(builder: ConverterActivityComponent.Builder): AndroidInjector.Factory<out Activity>
}