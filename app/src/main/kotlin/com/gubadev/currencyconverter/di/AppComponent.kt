package com.gubadev.currencyconverter.di

import android.app.Application
import com.gubadev.currencyconverter.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    AndroidInjectionModule::class,
    ActivityModule::class,
    ViewModelModule::class
])
interface AppComponent {

    fun inject(app: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(context: Application): Builder

        @BindsInstance
        fun baseUrl(baseUrl: String): Builder

        fun appModule(module: AppModule): Builder

        fun build(): AppComponent
    }
}