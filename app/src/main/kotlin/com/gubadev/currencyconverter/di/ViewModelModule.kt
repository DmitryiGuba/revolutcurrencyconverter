package com.gubadev.currencyconverter.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gubadev.currencyconverter.converter.ConverterViewModel
import com.gubadev.currencyconverter.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun viewModelFactory(provider: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ConverterViewModel::class)
    abstract fun converterViewModel(viewModel: ConverterViewModel): ViewModel
}