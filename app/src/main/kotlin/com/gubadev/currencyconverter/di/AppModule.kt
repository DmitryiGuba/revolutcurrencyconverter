package com.gubadev.currencyconverter.di

import android.app.Application
import android.content.Context
import com.google.gson.GsonBuilder
import com.gubadev.currencyconverter.R
import com.gubadev.data.net.CurrencyAPI
import com.gubadev.data.repository.CurrencyRepositoryImpl
import com.gubadev.domain.repository.CurrencyRepository
import com.gubadev.domain.usecases.CurrencyUpdateUseCase
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(subcomponents = [ConverterActivityComponent::class])
object AppModule {

    @Provides
    @Singleton
    fun provideOkHttpClient() = OkHttpClient.Builder()
        .followRedirects(false)
        .build()

    @Provides
    @Singleton
    fun provideGson() = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient, baseUrl: String) = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(client)
        .build()

    @Provides
    @Singleton
    fun provideCurrencyUpdateUseCase(repository: CurrencyRepositoryImpl) = CurrencyUpdateUseCase(repository)

    @Provides
    @Singleton
    fun provideAPI(retrofit: Retrofit) = retrofit.create(CurrencyAPI::class.java)

    @Provides
    @Singleton
    fun provideContext(app: Application): Context = app

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context) =
        context.getSharedPreferences(context.resources.getString(R.string.pref_name), Context.MODE_PRIVATE)
}