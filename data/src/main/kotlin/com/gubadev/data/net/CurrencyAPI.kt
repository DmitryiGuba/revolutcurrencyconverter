package com.gubadev.data.net

import com.gubadev.data.entities.CurrencyNet
import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyAPI {
    @GET(value = "/latest")
    fun getAll(@Query(value = "base") code: String): Maybe<CurrencyNet>
}