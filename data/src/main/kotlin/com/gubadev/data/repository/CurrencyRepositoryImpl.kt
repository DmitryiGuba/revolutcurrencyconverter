package com.gubadev.data.repository

import com.gubadev.data.entities.mapToCurrencies
import com.gubadev.data.net.CurrencyAPI
import com.gubadev.domain.entities.Currency
import com.gubadev.domain.repository.CurrencyRepository
import io.reactivex.Maybe
import javax.inject.Inject

class CurrencyRepositoryImpl @Inject constructor(private val api: CurrencyAPI) : CurrencyRepository {
    override fun load(code: String): Maybe<List<Currency>> =
        api.getAll(code).flatMap { Maybe.just(it.mapToCurrencies()) }
}