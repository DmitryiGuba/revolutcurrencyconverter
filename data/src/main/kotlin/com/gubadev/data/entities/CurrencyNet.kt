package com.gubadev.data.entities

import com.google.gson.annotations.Expose
import com.gubadev.domain.entities.Currency
import com.gubadev.domain.entities.CurrencyCodes
import java.lang.IllegalStateException

data class CurrencyNet(
    @Expose
    val base: String,
    @Expose
    val date: String,
    @Expose
    val rates: Map<String, Float>
)

fun CurrencyNet.mapToCurrencies() = rates
    .asSequence()
    .map { Currency(base.currencyCode(), it.key.currencyCode(), it.value) }
    .toMutableList()
    .apply {
        add(0, Currency(base.currencyCode(), base.currencyCode(), 1f))
    }

fun String.currencyCode() = when (this) {
    "EUR" -> CurrencyCodes.EUR
    "AUD" -> CurrencyCodes.AUD
    "BGN" -> CurrencyCodes.BGN
    "BRL" -> CurrencyCodes.BRL
    "CAD" -> CurrencyCodes.CAD
    "CHF" -> CurrencyCodes.CHF
    "CNY" -> CurrencyCodes.CNY
    "CZK" -> CurrencyCodes.CZK
    "DKK" -> CurrencyCodes.DKK
    "GBP" -> CurrencyCodes.GBP
    "HKD" -> CurrencyCodes.HKD
    "HRK" -> CurrencyCodes.HRK
    "HUF" -> CurrencyCodes.HUF
    "IDR" -> CurrencyCodes.IDR
    "ILS" -> CurrencyCodes.ILS
    "INR" -> CurrencyCodes.INR
    "ISK" -> CurrencyCodes.ISK
    "JPY" -> CurrencyCodes.JPY
    "KRW" -> CurrencyCodes.KRW
    "MXN" -> CurrencyCodes.MXN
    "MYR" -> CurrencyCodes.MYR
    "NOK" -> CurrencyCodes.NOK
    "NZD" -> CurrencyCodes.NZD
    "PHP" -> CurrencyCodes.PHP
    "PLN" -> CurrencyCodes.PLN
    "RON" -> CurrencyCodes.RON
    "RUB" -> CurrencyCodes.RUB
    "SEK" -> CurrencyCodes.SEK
    "SGD" -> CurrencyCodes.SGD
    "THB" -> CurrencyCodes.THB
    "TRY" -> CurrencyCodes.TRY
    "USD" -> CurrencyCodes.USD
    "ZAR" -> CurrencyCodes.ZAR
    else -> throw IllegalStateException("Unknown currency $this")
}