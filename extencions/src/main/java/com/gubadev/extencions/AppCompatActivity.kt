package com.gubadev.extencions

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate

var AppCompatActivity.darkMode: Boolean
    get() = isNightMode()
    set(value) {
        val mode = if (value) {
            AppCompatDelegate.MODE_NIGHT_YES
        } else {
            AppCompatDelegate.MODE_NIGHT_NO
        }
        if (isNightMode() != value) {
            recreate()
        }
        AppCompatDelegate.setDefaultNightMode(mode)
    }
