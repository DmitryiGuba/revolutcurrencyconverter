package com.gubadev.extencions

import android.content.Context
import android.content.res.Configuration
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat

fun Context.inflate(@LayoutRes id: Int, container: ViewGroup, isAttachedToRoot: Boolean = false) =
    LayoutInflater.from(this).inflate(id, container, isAttachedToRoot)

fun Context.getCurrentMode() = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK

fun Context.isNightMode() = when (getCurrentMode()) {
    Configuration.UI_MODE_NIGHT_YES -> true
    Configuration.UI_MODE_NIGHT_NO -> false
    Configuration.UI_MODE_NIGHT_UNDEFINED -> false
    else -> false
}

fun Context.color(@ColorRes id: Int) = ContextCompat.getColor(this, id)
