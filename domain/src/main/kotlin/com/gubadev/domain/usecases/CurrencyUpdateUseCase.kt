package com.gubadev.domain.usecases

import com.gubadev.domain.entities.Currency
import com.gubadev.domain.repository.CurrencyRepository
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers

class CurrencyUpdateUseCase(private val repository: CurrencyRepository) {
    fun update(code: String = "EUR"): Maybe<List<Currency>> = repository
        .load(code)
        .subscribeOn(Schedulers.io())
}