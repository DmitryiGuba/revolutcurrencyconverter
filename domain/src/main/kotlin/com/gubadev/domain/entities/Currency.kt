package com.gubadev.domain.entities

data class Currency(
    val base: CurrencyCodes,
    val code: CurrencyCodes,
    var value: Float,
    var amount: Float = 0f
)