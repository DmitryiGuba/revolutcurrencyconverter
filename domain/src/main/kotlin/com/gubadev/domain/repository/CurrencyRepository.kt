package com.gubadev.domain.repository

import com.gubadev.domain.entities.Currency
import io.reactivex.Maybe

interface CurrencyRepository {
    fun load(code: String): Maybe<List<Currency>>
}